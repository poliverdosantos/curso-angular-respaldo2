import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/servicios/usuario.service';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  pais:any;
  usuario2: any;
  femenino: boolean; 
  usuario: any;
  location:boolean;
  usuario3:any;
  datos:any;
  constructor(private servicioUsuario: UsuarioService) { 
    this.location= false
    this.femenino=false
  }

  
  ngOnInit(): void {
    this.servicioUsuario.obtenerUsuario().subscribe({
      next: user => {
        //console.log(user);
        this.usuario = user["results"][0];
      },
      error: error => {
        console.log(error);
      },
      complete: () => {
        console.log('solicitud completa');
      }
    })
  }

  showFemale(): void{
    this.servicioUsuario.obtenerUsuarioMasculino().subscribe({
      next: user => {
        console.log(user);
        this.usuario2 = user['results'][0];
        this.femenino = true;
      },
      error: error => {
        console.log(error);
      }
    });
  }

  /*obtener pais*/
    obtenerPais(): void{
    this.servicioUsuario.obtenerCiudad().subscribe({
      next: location => {
        console.log(location);
        this.pais = location['results'][0];
        this.location = true;
      },
      error: error => {
        console.log(error);
      }
    });
  }

  MostrarElementos(): void {
    this.servicioUsuario.obtenerCantidadElementos().subscribe({
      next: user => {
        console.log(user);
      },
      error: error => {
        console.log(error);
      }
    });
  }

  MostrarFoto():void{
    this.servicioUsuario.obtenerFoto().subscribe({
      next:user =>{
        console.log(user);
        this.usuario3=user[0];        
      },
      error: error => {
        console.log(error);
      }
    })
  }


  
  MostrarDatos():void{
    this.servicioUsuario.obtenerDatos().subscribe({
      next:user =>{
        console.log(user);
        this.datos=user[0];        
      },
      error: error => {
        console.log(error);
      }
    })
  }

}
