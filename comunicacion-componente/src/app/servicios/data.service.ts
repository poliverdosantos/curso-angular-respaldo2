import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  nombre = 'sin nombre';
  direccion:string='';

  private _datasource = new BehaviorSubject<string>('');
  //asObserbable convierte a un observable
  //datasource es una variable para suscribirse a la informacion
  
  datasource$ = this._datasource.asObservable();
  constructor() { }

  public ModificarDireccion(_direccion:string):void{
    this.direccion = _direccion;
    //cin next se coloca un valor al dataSource
    this._datasource.next(this.direccion);
  }
}
