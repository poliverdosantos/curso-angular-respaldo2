import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';



@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  mensaje = 'Mensaje del hijo al padre';

  @Input() ChildMessaje!:string;
  @Output() EventoMensaje = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
    console.log(this.ChildMessaje);
    this.EventoMensaje.emit(this.mensaje);    
  }

}
