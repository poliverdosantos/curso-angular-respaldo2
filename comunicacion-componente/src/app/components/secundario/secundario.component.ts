import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-secundario',
  templateUrl: './secundario.component.html',
  styleUrls: ['./secundario.component.css']
})
export class SecundarioComponent implements OnInit {


  direccion:string="AV Circunvalacion #123";

  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  }

  CambiarDireccion():void{
    this.dataService.ModificarDireccion(this.direccion)
  }


}
