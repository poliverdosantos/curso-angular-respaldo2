import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  dir!:string;

  constructor(dataService: DataService) { 
    dataService.datasource$.subscribe(data=>{
      console.log(data);
      this.dir = data ;      
    })
  }

  ngOnInit(): void {
  }

}
