import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    //Si entra a auth carga el loadChildren
    path: 'auth',
    //Cargar las rutas hijas, se cargan mediante el modulo, relacionado
    //a los componentes
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
  },
  {
    //Si entra a productos carga el loadChildren
    path: 'productos',
    //Cargar las rutas hijas, se cargan mediante el modulo, relacionado
    //a los componentes
    loadChildren: () => import('./productos/productos.module').then(m => m.ProductosModule),
  },



  {
    path:'**',
    redirectTo: ''
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
