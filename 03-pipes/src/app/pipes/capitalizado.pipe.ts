import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

  transform(value: string,todas:boolean=true): string {
    console.log(value);
    console.log(todas);

    value= value.toLowerCase();
    let nombres = value.split(" ");
    console.log(nombres);

    if (todas) {
      nombres = nombres.map(nombre =>{
        return nombre[0].toUpperCase()+ nombre.substring(1)
      } );
    }else{
      console.log(nombres);
      console.log(nombres[0][0].toUpperCase());
      // nombres[0][0].toUpperCase() el primer [0] obtiene el elemento de la posicion 0, el segundo [0]
      // obtiene el caracter de la posicion 0     
      nombres[0] =  nombres[0][0].toUpperCase() + nombres[0].substring(1);
    }

    return nombres.join(' ');
  }

}
