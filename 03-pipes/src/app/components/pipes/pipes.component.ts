import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {

  nombre = 'Ramiro Santos';
  array = [1,2,3,4,5,6,7,8,9,10];
  PI = Math.PI;
  porcentaje:number=0.234;
  salario = 1234.5;
  fecha= new Date();
  nombre2:string='MaxiMiliAno PaReDes';

  heroe:any={
    nombre:'logan',
    clave:'wolverine',
    edad:500,
    direccion:{
    calle:'av Circunvalacion #121',
    zona:'Los Molinos'
    }
  };

  // setTimeout es el tiempo que va esperar para ejecutarse
  valorPromesa = new Promise<string>((resolve) => {
    setTimeout(() => {
      resolve('Llego la data');
    }, 3500);
  });


  constructor() { }

  ngOnInit(): void {
  }

}
