import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, DoCheck, OnChanges, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnChanges,DoCheck, AfterContentInit,
AfterContentChecked, AfterViewInit, AfterViewChecked,
OnDestroy  {

  constructor() { 
    console.log('constructor');    
  }

  ngOnInit(): void {
    console.log('ngOnInit');    
  }

  ngOnChanges():void{
    console.log('OnChanges');    
  }

  ngDoCheck():void{
    console.log('DoCheck');
  }

  ngAfterContentInit(){
    console.log('AfterContentInit');    
  }

  ngAfterContentChecked(): void {
    console.log('AfterContentChecked');
  }

  ngAfterViewInit(){
    console.log('AfterViewInit');    
  }

  ngAfterViewChecked(): void {
    console.log('ngAfterViewChecked');    
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy');
    
  }





}
