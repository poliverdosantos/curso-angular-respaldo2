import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Heroe, HeroesService } from 'src/app/servicios/heroes.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  heroes: any []=[];
  termino!:string;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private heroesService: HeroesService) { }

  ngOnInit(): void {
    /*buscador de terminos*/
    this.activatedRoute.params.subscribe(params=>{
      console.log(params['termino']);  
      this.termino =params['termino'];
      //sospechoso de falla
      // this.heroes= this.heroesService.buscarHeroes(params['termino']);    
      this.heroes = this.heroesService.buscarHeroes(this.termino);
      console.log(this.heroes);
      
    })
  }

  verHeroe(idx:number){
    console.log(idx);
    this.router.navigate(['/heroe',idx])

  }

}
