import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeroesService,Heroe } from "../../servicios/heroes.service";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes : any[]=[];

  constructor(private _HeroesService: HeroesService,
              private router : Router) { 
    console.log('constructor');
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.heroes = this._HeroesService.getHeroes();
    console.log(this.heroes);
    
  }
/*es aconsejable que toda la logica este en el ts y no en el html*/
   verHeroe(idx: number):void {
    console.log(idx);
    this.router.navigate(['/heroe',idx]);    
   }
}
